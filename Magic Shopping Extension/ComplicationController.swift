//
//  ComplicationController.swift
//  Magic Shopping Extension
//
//  Created by Bill Martensson on 2017-10-31.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        // Call the handler with the current timeline entry
        
        if(complication.family == .circularSmall)
        {
            var temp = CLKComplicationTemplateCircularSmallSimpleText()
            temp.textProvider = CLKSimpleTextProvider(text: "Hejsan", shortText: "ABC")
            
            let timeEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: temp)
            
            handler(timeEntry)
            return
        }
        
        if(complication.family == .modularSmall)
        {
            var temp = CLKComplicationTemplateModularSmallSimpleText()
            temp.textProvider = CLKSimpleTextProvider(text: "Tjena", shortText: "XYZ")
            
            let timeEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: temp)
            
            handler(timeEntry)
            return
        }
        
        if(complication.family == .modularLarge)
        {
            let template = CLKComplicationTemplateModularLargeStandardBody()
            template.headerTextProvider = CLKSimpleTextProvider(text: "Shopping")
            template.body1TextProvider = CLKSimpleTextProvider(text: "Köp")
            template.body2TextProvider = CLKSimpleTextProvider(text: "123")
            let timelineEntry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: template)
            handler(timelineEntry)
            
            return
        }
        
        
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Placeholder Templates
    
    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        // This method will be called once per supported complication, and the results will be cached
        handler(nil)
    }
    
}
