//
//  InterfaceController.swift
//  Magic Shopping Extension
//
//  Created by Bill Martensson on 2017-10-31.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import WatchKit
import Foundation

import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet var theLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        } else {
            print("WCSession.isSupported NOT TRUE")
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    @IBAction func clickingButton() {
        if (WCSession.default.isReachable) {
            // this is a meaningless message, but it's enough for our purposes
            let message = ["frukt": "apelsin"]
            //WCSession.default.sendMessage(message, replyHandler: nil)
            
            do {
                try WCSession.default.updateApplicationContext(message)
            } catch {
                print("FAIL UPDATE APP CONTEXT")
            }
            
            
            
        } else {
            print("REACHABLE FAIL")
        }
    }
    
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if(error == nil)
        {
            print("ACT OK")
            
            
        } else {
            print("ACT FAIL")
        }
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        if let frukt = message["frukt"] as? String
        {
            theLabel.setText(frukt)
        }
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        
        if let frukt = applicationContext["frukt"] as? String
        {
            DispatchQueue.main.async {
                self.theLabel.setText(frukt)
            }
        }
        
        
    }
    
}
