//
//  ShoppingListsInterfaceController.swift
//  Magic Shopping Extension
//
//  Created by Bill Martensson on 2017-10-31.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import WatchKit
import Foundation

import WatchConnectivity

class ShoppingListsInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    
    @IBOutlet var shoppingTable: WKInterfaceTable!
    
    var shoppingLists = [String]()
    var shoppingBought = [Bool]()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        } else {
            print("WCSession.isSupported NOT TRUE")
        }
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if(error == nil)
        {
            print("ACT OK")
            
            updateTableWithData()
            
        } else {
            print("ACT FAIL")
        }
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        
        updateTableWithData()
        
    }
    
    func updateTableWithData()
    {
        print(WCSession.default.receivedApplicationContext)
        
        if let bought = WCSession.default.receivedApplicationContext["shop_bought"] as? [Bool]
        {
            self.shoppingBought = bought
        } else {
            return
        }
        
        if let lists = WCSession.default.receivedApplicationContext["shop_name"] as? [String]
        {
            
            self.shoppingLists = lists
            
            DispatchQueue.main.async {
                self.shoppingTable.setNumberOfRows(self.shoppingLists.count, withRowType: "shoprow")
                
                for (index, name) in self.shoppingLists.enumerated() {
                    let row = self.shoppingTable.rowController(at: index) as! ShoppingListsRow
                    
                    if(self.shoppingBought[index] == true)
                    {
                        row.listnameLabel.setText(name+" X")
                    } else {
                        row.listnameLabel.setText(name+" O")
                    }
                    
                }
                
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        
        if(shoppingBought[rowIndex] == true)
        {
            shoppingBought[rowIndex] = false
        } else {
            shoppingBought[rowIndex] = true
        }
        
        var message = [String : Any]()
        
        message["shop_bought"] = shoppingBought
        
        do {
            try WCSession.default.updateApplicationContext(message)
            print("UPDATE APP CONTEXT OK")
        } catch let err as Error {
            print("FAIL UPDATE APP CONTEXT")
            print(err.localizedDescription)
        }
        
        
    }
    
}
