//
//  AllVC.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-11-09.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation

extension UIViewController
{
    
    func doFunStuff()
    {
        
    }
    
}

final class AllVC
{
    
    static func doAnalyticsScreen(screenName: String)
    {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: screenName)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        GAI.sharedInstance().dispatch()
        
    }
    
    static func doAnalyticsEvent()
    {
        
        
    }
    
    static func getImage(_ imageUrl: String, fbkey : String, completion: @escaping (_ result: UIImage?) -> Void)
    {
        
        // Image name from url
        var imageName = fbkey
        
        
        if(imageUrl == "")
        {
            completion(nil)
            return
        }
        
        let tempFile = try! URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imageName)
        
        let checkValidation = FileManager.default
        
        print("CHECK EXIST")
        print(tempFile.path)
        if(checkValidation.fileExists(atPath: tempFile.path))
        {
            let imageData = try? Data(contentsOf: tempFile)
            
            completion(UIImage(data: imageData!))
            return
        }
        
        let request = NSMutableURLRequest(url: URL(string: imageUrl)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if(error != nil)
            {
                completion(nil)
                return
            }
            
            try? data?.write(to: URL(fileURLWithPath: tempFile.path), options: [.atomicWrite])
            
            completion(UIImage(data: data!))
            return
        }) // var task
        task.resume()
        
    }
}
