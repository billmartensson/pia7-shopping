//
//  CameraViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-10-24.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var error: NSError?
    
    
    var camera : UIImagePickerController?
    
    @IBOutlet weak var theImage: UIImageView!
    
    
    @IBOutlet weak var redPreview: UIView!
    
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var allThePictures = [UIImage]()
    
    
    
    
    var photosAsset: PHFetchResult<PHAsset>!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        
        
        let devices = AVCaptureDevice.devices().filter{ ($0 as AnyObject).hasMediaType(AVMediaType.video) && ($0 as AnyObject).position == AVCaptureDevice.Position.back }
        
        if let captureDevice = devices.first as? AVCaptureDevice  {
            
            do {
                
                captureSession.addInput(try AVCaptureDeviceInput(device: captureDevice))
            } catch {
                
            }
            
            captureSession.sessionPreset = AVCaptureSession.Preset.photo
            captureSession.startRunning()
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecType.jpeg]
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            
            
            var previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
            previewLayer.bounds = view.bounds
            previewLayer.position = CGPoint(x: redPreview.bounds.midX, y: redPreview.bounds.midY)
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            let cameraPreview = UIView(frame: CGRect(x: 0.0, y: 0.0, width: redPreview.bounds.size.width, height: redPreview.bounds.size.height))
            cameraPreview.layer.addSublayer(previewLayer)
            let tapping = UITapGestureRecognizer(target: self, action:#selector(saveToCamera(sender:)))
            
            tapping.numberOfTapsRequired = 1
            
            cameraPreview.addGestureRecognizer(tapping)
            redPreview.addSubview(cameraPreview)
        
        }
        
    }
    
    func requestAuthorizationHandler(_ status: PHAuthorizationStatus)
    {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized
        {
            let fetchOptions: PHFetchOptions = PHFetchOptions()
            
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            
            photosAsset = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
            
            print(photosAsset.count)
            
            getAphoto()
            
        }
        else
        {
            // ERROR NO ACCESS
        }
    }
    
    func getAphoto()
    {
        let imgAsset: PHAsset = photosAsset[0]
        
        let imageOptions = PHImageRequestOptions()
        imageOptions.isSynchronous = true
        
        let newSize = CGSize(width: 120, height: 120)
        
        PHImageManager.default().requestImage(for: imgAsset, targetSize: newSize, contentMode: PHImageContentMode.aspectFill, options: imageOptions, resultHandler: { (result, info) -> Void in
            
            print("GOT IMAGE")
            
            DispatchQueue.main.async {
                self.theImage.image = result
            }
            
            
            
            // result = UIImage
        })
    }
    
    @objc func saveToCamera(sender: UITapGestureRecognizer) {
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)
                
                self.theImage.image = UIImage(data: imageData!)
                
                self.allThePictures.append(UIImage(data: imageData!)!)
                
                self.photosCollectionView.reloadData()
                
                //UIImageWriteToSavedPhotosAlbum(UIImage(data: imageData!)!, nil, nil, nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func getFromCamera(_ sender: Any) {
        camera = UIImagePickerController()
        camera!.delegate = self
        camera!.sourceType = UIImagePickerControllerSourceType.camera
        camera!.allowsEditing = false
        camera!.showsCameraControls = false
        
        let screenSize = UIScreen.main.bounds.size
        
        let camHeight = screenSize.width * (4.0/3.0)
        let scaleUp = screenSize.height/camHeight
        
        self.camera!.cameraViewTransform = CGAffineTransform(scaleX: scaleUp, y: scaleUp)
        
        
        let cameraOverleyView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        
        let cameraBottomView = UIView(frame: CGRect(x: 0, y: screenSize.width, width: screenSize.width, height: screenSize.height-screenSize.width))
        cameraBottomView.backgroundColor = UIColor.white
        
        cameraOverleyView.addSubview(cameraBottomView)
        
        
        let takePictureButton = UIButton(frame: CGRect(x: 50, y: 50, width: 75, height: 50))
        takePictureButton.setTitle("Take", for: UIControlState())
        takePictureButton.backgroundColor = UIColor.red
        
        takePictureButton.addTarget(self, action: #selector(self.cameraTakePicture(_:)), for: UIControlEvents.touchUpInside)
        
        
        let doneButton = UIButton(frame: CGRect(x: 150, y: 50, width: 75, height: 50))
        doneButton.setTitle("Done", for: UIControlState())
        
        doneButton.backgroundColor = UIColor.red
        
        doneButton.addTarget(self, action: #selector(self.cameraDone(_:)), for: UIControlEvents.touchUpInside)
        
        cameraBottomView.addSubview(takePictureButton)
        cameraBottomView.addSubview(doneButton)
        
        self.camera!.cameraOverlayView = cameraOverleyView
        
        
        self.present(camera!, animated: true, completion: nil)
        
        
    }
    
    @objc func cameraTakePicture(_ sender: UIButton!)
    {
        self.camera!.takePicture()
    }
    
    @objc func cameraDone(_ sender: UIButton!)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func getFromGallery(_ sender: Any) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        pickedImage = cropToSquare(image: pickedImage)
        
        theImage.image = pickedImage
        
        //self.dismiss(animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    
    
    
    func cropToSquare(image originalImage: UIImage) -> UIImage {
        
        UIGraphicsBeginImageContext(originalImage.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.rotate(by: 0)
        
        originalImage.draw(at: CGPoint(x: 0, y: 0))
        
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        //let t = CGAffineTransformMakeRotation(90/(180 * M_PI))
        
        // Create a copy of the image without the imageOrientation property so it is in its native orientation (landscape)
        let contextImage: UIImage = UIImage(cgImage: rotatedImage!.cgImage!)
        
        // Get the size of the contextImage
        let contextSize: CGSize = contextImage.size
        
        let posX: CGFloat
        let posY: CGFloat
        let width: CGFloat
        let height: CGFloat
        
        // Check to see which length is the longest and create the offset based on that length, then set the width and height of our rect
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            width = contextSize.height
            height = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            width = contextSize.width
            height = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: width, height: height)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        //let image: UIImage = UIImage(CGImage: imageRef, scale: originalImage.scale, orientation: originalImage.imageOrientation)
        let image: UIImage = UIImage(cgImage: imageRef)
        
        return resizeImage(image, newWidth: 600)
        //return scaleImage(image, toSize: CGSize(width: 600, height: 600))
    }
    
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return allThePictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photocell", for: indexPath) as! PhotoCollectionViewCell
        
        cell.thePhoto.image = allThePictures[indexPath.item]
        
        return cell
        
    }
    
}
