//
//  LoginViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit


class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func login(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextfield.text!, password: passwordTextfield.text!) {
            (user, error) in
            
            if(error != nil)
            {
                print("FEL VID LOGIN")
                
                let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Login error title") , message: NSLocalizedString("Could not login in", comment: "Login error message"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                    // Tryckt ok
                }
                
                alert.addAction(okAction)
                
                self.present(alert, animated: true, completion:nil)
            } else {
                print("OK LOGIN")
                
                Analytics.logEvent(AnalyticsEventLogin, parameters: nil)
                
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func signup(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!) {
            (user, error) in
            
            if(error != nil)
            {
                print("FEL VID SIGNUP")
            } else {
                print("OK SIGNUP")
                
                Analytics.logEvent(AnalyticsEventSignUp, parameters: nil)
                
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func forgotpassword(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: emailTextfield.text!) { (error) in
            
            
            
        }
    }
    
    
    
    @IBAction func facebookLogin(_ sender: Any) {
        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        if(FBSDKAccessToken.current() != nil)
        {
            // Logga ut
            fbLoginManager.logOut()
            
        } else {
            // Logga in
            fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) -> Void in
                
                if(error != nil)
                {
                    print("LOGIN ERROR")
                } else {
                    print("LOGIN OK")
                    
                    let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                    
                    Auth.auth().signIn(with: credential) { (user, error) in
                        if let error = error {
                            print("FIREBASE FACEBOOK ERROR LOGIN")
                            return
                        }
                        print("OK FB/FB LOGIN")
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                
                    
                }
                
            }
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
