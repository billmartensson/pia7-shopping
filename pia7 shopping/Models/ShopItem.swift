//
//  ShopItem.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCore

class ShopItem
{
    var my_list : ShopList?
    
    var fbKey = ""
    
    var shopname = ""
    var amount = 0
    var bought = false
    
    var image : UIImage?
    
    func getPrettyAmount() -> String
    {
        if(amount == 0)
        {
            return ""
        }
        
        if(amount > 100)
        {
            return "Massor många"
        }
        
        if(amount > 50)
        {
            return "Många"
        }
        
        return "\(amount) st"
    }
    
    func toggleBought()
    {
        var addingShopItem = [String : Any]()
        
        if(self.bought == true)
        {
            addingShopItem["bought"] = false
            self.bought = false
        } else {
            addingShopItem["bought"] = true
            self.bought = true
        }
        let ref = Database.database().reference()
        
        var fb_save = ref.child("pia7shopping").child(my_list!.getListOwner())
        fb_save = fb_save.child(my_list!.fb_key!).child("items")
        fb_save = fb_save.child(self.fbKey)
        
        
        fb_save.updateChildValues(addingShopItem) { (error, ref) in
            if(error != nil)
            {
                print(error.debugDescription)
            }
        }
    }
    
    func save(completion: @escaping (Bool) -> Void)
    {
        var addingShopItem = [String : Any]()
        
        addingShopItem["name"] = self.shopname
        addingShopItem["amount"] = self.amount
        addingShopItem["bought"] = self.bought
        
        let ref = Database.database().reference()
        
        var fb_add = ref.child("pia7shopping").child(self.my_list!.getListOwner())
        fb_add = fb_add.child(self.my_list!.fb_key!).child("items")
        if(self.fbKey == "")
        {
            fb_add = fb_add.childByAutoId()
        } else {
            fb_add = fb_add.child(self.fbKey)
        }
        
        fb_add.setValue(addingShopItem) { (error, ref) in
            if(error == nil)
            {
                // OK spara
                completion(true)
                return
            } else {
                completion(false)
                return
            }
        }
    }
    
    func delete(completion: @escaping (Bool) -> Void)
    {
        let ref = Database.database().reference()
        
        var fb_delete = ref.child("pia7shopping").child(self.my_list!.getListOwner())
        fb_delete = fb_delete.child(self.my_list!.fb_key!).child("items")
        fb_delete = fb_delete.child(self.fbKey)
        
        fb_delete.removeValue() { (error, ref) in
            if(error == nil)
            {
                self.my_list!.getShoppingItems() {(result : Bool) in
                    completion(result)
                    return
                }
                return
            } else {
                completion(false)
                return
            }
            
        }
    }
    
    func getItemImage(completion: @escaping (UIImage?) -> Void)
    {
        if(self.image != nil)
        {
            completion(self.image)
            return
        }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let itemRef = storageRef.child("pia7shopping/"+self.fbKey)
        
        /*
         itemRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
         if let error = error {
         // Uh-oh, an error occurred!
         print("ERROR HÄMTA BILD")
         } else {
         let downloadedImage = UIImage(data: data!)
         cell.itemImage.image = downloadedImage
         }
         }
         */
        
        itemRef.downloadURL { url, error in
            if let error = error {
                // Handle any errors
            } else {
                // Get the download URL for 'images/stars.jpg'
                
                AllVC.getImage(url!.absoluteString, fbkey: self.fbKey) {(result: UIImage?) in
                    
                    DispatchQueue.main.async {
                        self.image = result
                        completion(result)
                        return
                    }
                }
                
            }
        }
    }
}
