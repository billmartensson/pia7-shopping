//
//  ShopList.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-11-09.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCore


class ShopList
{
    var name : String?
    var fb_key : String?
    var owner : String?
    
    var items : [ShopItem]?
    
    
    func getListOwner() -> String
    {
        var list_owner = Auth.auth().currentUser?.uid
        if self.owner != nil
        {
            list_owner = self.owner
        }
        
        return list_owner!
    }
    
    
    func createList(item : DataSnapshot)
    {
        let thisItem = item.value as! NSDictionary
        
        self.name = (thisItem["listname"] as! String)
        self.fb_key = item.key
        
        if(thisItem.value(forKey: "owner") == nil)
        {
            self.owner = Auth.auth().currentUser!.uid
        } else {
            self.owner = (thisItem.value(forKey: "owner") as! String)
        }
        
    }
    
    func save(completion: @escaping (Bool) -> Void)
    {
        let ref = Database.database().reference()
        
        var addingShopList = [String : Any]()
        addingShopList["listname"] = name
        
        var save_to = ref.child("pia7shopping").child(Auth.auth().currentUser!.uid)
        
        if(self.fb_key == nil)
        {
            // SKAPA NY
            save_to = save_to.childByAutoId()
        } else {
            // SPARA ÖVER GAMMAL
            save_to = save_to.child(self.fb_key!)
        }
        
        save_to.updateChildValues(addingShopList) { (error, ref) in
            // YEY UPPDATERAT PÅ FB
            if(error == nil)
            {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func delete(completion: @escaping (Bool) -> Void)
    {
        let ref = Database.database().reference()
        
        var fb_delete = ref.child("pia7shopping").child(Auth.auth().currentUser!.uid)
        fb_delete = fb_delete.child(self.fb_key!)
        
        fb_delete.removeValue() { (error, ref) in
            if(error == nil)
            {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func invite(username : String, completion: @escaping (Bool) -> Void)
    {
        let ref = Database.database().reference()
        
        // HITTA ANVÄNDARE
        ref.child("pia7shoppingUsers").queryOrdered(byChild: "username").queryEqual(toValue: username).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user
            var userKey : String?
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                let thisItem = item.value as! NSDictionary
                
                userKey = item.key
                
                //Messaging.messaging().sendMessage(["Alert": "Invited to shoppinglist"], to: thisItem.value(forKey: "token") as! String, withMessageID: "", timeToLive: 999999999999)
                
            }
            
            if(userKey != nil)
            {
                // GE SHOPPINGLISTA TILL ANVÄNDAREN
                ref.child("pia7shopping").child(userKey!).child(self.fb_key!).setValue(["owner": Auth.auth().currentUser?.uid, "listname": self.name]) { (error, ref) in
                        if(error == nil)
                        {
                            completion(true)
                            return
                        } else {
                            completion(false)
                            return
                        }
                }
            } else {
                completion(false)
                return
            }
            
        }) { (error) in
            print(error.localizedDescription)
            completion(false)
            return
        }
    }
    
    
    func getShoppingItems(completion: @escaping (Bool) -> Void)
    {
        items = [ShopItem]()
        
        // Do any additional setup after loading the view.
        let ref = Database.database().reference()
        
        var fb_get = ref.child("pia7shopping").child(self.owner!)
        fb_get = fb_get.child(self.fb_key!).child("items")
        
        fb_get.observeSingleEvent(of: .value, with: { (snapshot) in
            // Get shopping list
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                let thisItem = item.value as! NSDictionary
                
                let tempShop = ShopItem()
                tempShop.fbKey = item.key
                tempShop.shopname = thisItem["name"] as! String
                tempShop.amount = thisItem["amount"] as! Int
                tempShop.bought = thisItem["bought"] as! Bool
                tempShop.my_list = self
                
                self.items!.append(tempShop)
            }
            
            /*
            var message = [String : Any]()
            var shop_name = [String]()
            var shop_bought = [Bool]()
            
            for si in self.shoppingData
            {
                shop_name.append(si.shopname)
                shop_bought.append(si.bought)
            }
            message["shop_name"] = shop_name
            message["shop_bought"] = shop_bought
            
            do {
                try WCSession.default.updateApplicationContext(message)
                print("UPDATE APP CONTEXT OK")
            } catch let err as Error {
                print("FAIL UPDATE APP CONTEXT")
                print(err.localizedDescription)
            }
            */
            
            completion(true)
            return
            
        }) { (error) in
            print(error.localizedDescription)
            completion(false)
            return
        }
    }
    
    func addItem(name : String, amount : Int, completion: @escaping (Bool) -> Void)
    {
        var addingShopItem = [String : Any]()
        
        addingShopItem["name"] = name
        addingShopItem["amount"] = amount
        addingShopItem["bought"] = false
        
        let ref = Database.database().reference()
        
        var fb_add = ref.child("pia7shopping").child(self.owner!)
        fb_add = fb_add.child(self.fb_key!).child("items").childByAutoId()
        
        fb_add.setValue(addingShopItem) { (error, ref) in
            if(error == nil)
            {
                // OK spara
                // Hämta alla items
                self.getShoppingItems() {(result: Bool) in
                    completion(result)
                }
            } else {
                completion(false)
                return
            }
        }
    }
    
    
}
