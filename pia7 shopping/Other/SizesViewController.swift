//
//  SizesViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class SizesViewController: UIViewController {

    
    @IBOutlet weak var bluebox: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        showStuff()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        print("BYTA STORLEK")
        
        showStuff()
        
    }
    
    func showStuff()
    {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            bluebox.isHidden = true
        } else {
            print("Portrait")
            bluebox.isHidden = false
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
