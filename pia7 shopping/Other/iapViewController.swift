//
//  iapViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-28.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import StoreKit

class iapViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    
    @IBOutlet weak var p1title: UILabel!
    @IBOutlet weak var p1description: UILabel!
    
    @IBOutlet weak var p1price: UILabel!
    
    @IBOutlet weak var p2title: UILabel!
    @IBOutlet weak var p2description: UILabel!
    
    @IBOutlet weak var p2price: UILabel!
    
    
    @IBOutlet weak var buy1button: UIButton!
    
    @IBOutlet weak var buy2button: UIButton!
    
    let productIdentifiers = Set(["bonuscontent", "golddiamond"])
    var productsArray = Array<SKProduct>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SKPaymentQueue.default().add(self)
        
        requestProductData()
        
        
        
        let defaults = UserDefaults.standard
        
        // Spara
        //defaults.set("Bill", forKey: "name")
        
        // Hämta
        if let name = defaults.value(forKey: "name") as? String
        {
            print("NAME: \(name)")
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buyProduct1(_ sender: Any) {
        let payment = SKPayment(product: productsArray[0])
        SKPaymentQueue.default().add(payment)
    }
    
    
    @IBAction func buyProduct2(_ sender: Any) {
        let payment = SKPayment(product: productsArray[1])
        SKPaymentQueue.default().add(payment)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func requestProductData()
    {
        if SKPaymentQueue.canMakePayments() {
            let request = SKProductsRequest(productIdentifiers: self.productIdentifiers as Set<String>)
            request.delegate = self
            request.start()
        } else {
            // Kan ej köpa
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        var products = response.products
        
        if (products.count != 0) {
            for i in 0 ..< products.count
            {
                self.productsArray.append(products[i])
                print("*****************")
                print(products[i].localizedTitle)
                print(products[i].localizedDescription)
                print(products[i].price)
                print(products[i].priceLocale)
                
            }
            
            let currency_format = NumberFormatter()
            currency_format.numberStyle = NumberFormatter.Style.currency
            currency_format.locale = products[0].priceLocale
            
            p1title.text = self.productsArray[0].localizedTitle
            p1price.text = currency_format.string(from: products[0].price)!
            p1description.text = self.productsArray[0].localizedDescription
            
            p2title.text = self.productsArray[1].localizedTitle
            p2price.text = currency_format.string(from: products[1].price)!
            p2description.text = self.productsArray[1].localizedDescription
            
            buy1button.isEnabled = true
            buy2button.isEnabled = true
            
        } else {
            print("No products found")
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("paymentQueue updatedTransactions")
        
        for transaction in transactions {
            
            switch transaction.transactionState {
                
            case SKPaymentTransactionState.purchased:
                print("Transaction Approved")
                print("Product Identifier: \(transaction.payment.productIdentifier)")
                self.deliverProduct(transaction)
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case SKPaymentTransactionState.failed:
                print("Transaction Failed")
                print(transaction.error.debugDescription)
                SKPaymentQueue.default().finishTransaction(transaction)
            default:
                break
            }
        }
    }
    
    func deliverProduct(_ transaction:SKPaymentTransaction) {
        print("deliverProduct "+transaction.payment.productIdentifier)
        if transaction.payment.productIdentifier == "bonuscontent"
        {
            print("bonuscontent bought")
        }
        else if transaction.payment.productIdentifier == "golddiamond"
        {
            print("golddiamond bought")
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("Transactions Restored")
        
        for transaction:SKPaymentTransaction in queue.transactions
        {
            if transaction.payment.productIdentifier == "bonuscontent"
            {
                print("bonuscontent Restored")
            }
            else if transaction.payment.productIdentifier == "golddiamond"
            {
                print("golddiamond Restored")
            }
        }
    }

}
