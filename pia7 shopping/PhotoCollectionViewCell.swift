//
//  PhotoCollectionViewCell.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-10-24.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var thePhoto: UIImageView!
    
    
}
