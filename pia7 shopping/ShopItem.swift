//
//  ShopItem.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-21.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation

class ShopItem
{
    var fbKey = ""
    
    var shopname = ""
    var amount = 0
    var bought = false
    
    func getPrettyAmount() -> String
    {
        if(amount == 0)
        {
            return ""
        }
        
        if(amount > 100)
        {
            return "Massor många"
        }
        
        if(amount > 50)
        {
            return "Många"
        }
        
        return "\(amount) st"
    }
    
    func toggleBought()
    {
        if(bought == true)
        {
            bought = false
        } else {
            bought = true
        }
    }
}
