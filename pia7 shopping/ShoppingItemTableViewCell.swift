//
//  ShoppingItemTableViewCell.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-10-24.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class ShoppingItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemImage: UIImageView!
    
    
    @IBOutlet weak var itemLabel: UILabel!
    
    @IBOutlet weak var itemBought: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
