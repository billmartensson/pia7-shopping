//
//  ShoppingListsTableview.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-11-09.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation

extension ShoppingListsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "list")!
        
        cell.textLabel?.text = shoppingLists[indexPath.row].name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "shopping", sender: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let modifyAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Edit action ...")
            
            self.listnameTextfield.text = self.shoppingLists[indexPath.row].name
            self.addButton.setTitle("Save", for: .normal)
            self.editingRow = indexPath.row
            
            success(true)
        })
        modifyAction.backgroundColor = .blue
        
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Delete action ...")
            
            self.shoppingLists[indexPath.row].delete() {(result: Bool) in
                if(result == true)
                {
                    self.getLists()
                } else {
                    print("ERROR DELETE")
                }
            }
            
            
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            
            guard let builder = GAIDictionaryBuilder.createEvent(withCategory: "shoppinglists", action: "delete", label: "", value: 0) else { return }
            tracker.send(builder.build() as [NSObject : AnyObject])
            
            GAI.sharedInstance().dispatch()
            
            
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
        
    }
    
}
