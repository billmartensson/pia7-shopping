//
//  ShoppingListsViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-10-10.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit


class ShoppingListsViewController: UIViewController {
    
    @IBOutlet weak var listnameTextfield: UITextField!
    @IBOutlet weak var listsTableview: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var inviteTextfield: UITextField!
    
    
    var ref: DatabaseReference!

    var shoppingLists = [ShopList]()
    
    var editingRow : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
    }

    override func viewDidAppear(_ animated: Bool) {
        Messaging.messaging().subscribe(toTopic: "shopping")
        
        if(Auth.auth().currentUser == nil)
        {
            shoppingLists.removeAll()
            listsTableview.reloadData()
            
            performSegue(withIdentifier: "login", sender: nil)
        } else {
            
            if(Messaging.messaging().fcmToken == nil)
            {
                print("NEJ! INGEN PUSH TOKEN")
            } else {
                print("YEY! PUSH TOKEN ÄR "+Messaging.messaging().fcmToken!)
                ref.child("pia7shoppingUsers").child(Auth.auth().currentUser!.uid).updateChildValues(["token": Messaging.messaging().fcmToken!]) { (error, ref) in
                }
            }
            
            AllVC.doAnalyticsScreen(screenName: "shoppingLists")
            
            getLists()
        }
        
        
    }
    
    
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            
            fbLoginManager.logOut()
            
            shoppingLists.removeAll()
            listsTableview.reloadData()
            
            performSegue(withIdentifier: "login", sender: nil)
        } catch {
            print("KUNDE INTE LOGGA UT")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addList(_ sender: Any) {
        if(listnameTextfield.text == "")
        {
            return
        }
        
        var list_to_save = ShopList()
        if(editingRow != nil)
        {
            list_to_save = shoppingLists[editingRow!]
        }
        list_to_save.name = listnameTextfield.text
        list_to_save.save() {(result: Bool) in
            if(result == true)
            {
                self.editingRow = nil
                self.addButton.setTitle("Add", for: .normal)
                
                self.getLists()
            } else {
                print("SAVE ERROR!!")
            }
        }
        
        listnameTextfield.text = ""

        
        
        Messaging.messaging().subscribe(toTopic: "addedlist")
        
        Analytics.logEvent("createdShoppingList", parameters: [
            AnalyticsParameterItemID: "id-XXXX" as NSObject,
            AnalyticsParameterItemName: "NAMEXXXX" as NSObject,
            AnalyticsParameterContentType: "contXXX" as NSObject
            
            ])
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        
        guard let builder = GAIDictionaryBuilder.createEvent(withCategory: "shoppinglists", action: "add", label: "", value: 0) else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        GAI.sharedInstance().dispatch()
        
    }
    
    
    func getLists()
    {
        shoppingLists.removeAll()
        
        // Do any additional setup after loading the view.
     ref.child("pia7shopping").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get shopping list
            for item in snapshot.children.allObjects as! [DataSnapshot]
            {
                let temp_list = ShopList()
                temp_list.createList(item: item)
                self.shoppingLists.append(temp_list)
            }
        
            self.listsTableview.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "shopping")
        {
            let dest = segue.destination as! ShoppinglistViewController
            
            dest.shoppingList = shoppingLists[sender as! Int]
        }
        
    }
    
    
    
    @IBAction func inviteToList(_ sender: Any) {
        if(editingRow == nil)
        {
            return
        }
        
        shoppingLists[editingRow!].invite(username: inviteTextfield.text!) {(result: Bool) in
            if(result == true)
            {
                print("OK INVITE")
                self.listnameTextfield.text = ""
                self.inviteTextfield.text = ""
                self.editingRow = nil
                
                self.addButton.setTitle("Add", for: .normal)
                self.view.endEditing(true)
            } else {
                print("ERROR INVITE")
            }
        }
    }
    
}
