//
//  ShoppingdetailViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-12.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import FBAudienceNetwork
import Firebase
import FirebaseStorage

class ShoppingdetailViewController: UIViewController, FBInterstitialAdDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    var s_item : ShopItem?
    var interAd : FBInterstitialAd?
    
    @IBOutlet weak var itemImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        AllVC.doAnalyticsScreen(screenName: "itemDetail")
        
        
        
        
        //itemLabel.text = s_item!.shopname+" "+String(s_item!.amount)
        
        
        interAd = FBInterstitialAd(placementID: "172870736609170_172879279941649")
        
        interAd?.delegate = self
        
        interAd?.load()
    }

    override func viewDidAppear(_ animated: Bool) {
        
        s_item?.getItemImage() {(result : UIImage?) in
            self.itemImage.image = result
        }
    }
    
    
    @IBAction func photoFromCamera(_ sender: Any) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBAction func photoFromGallery(_ sender: Any) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        pickedImage = cropToSquare(image: pickedImage)
        
        itemImage.image = pickedImage
        
        self.dismiss(animated: true, completion: nil);
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let itemRef = storageRef.child("pia7shopping/"+s_item!.fbKey)
        
        let imageData = UIImageJPEGRepresentation(pickedImage, 0.8)!
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = itemRef.putData(imageData, metadata: metadata) { (metadata, error) in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                return
            }
            // Metadata contains file metadata such as size, content-type, and download URL.
            let downloadURL = metadata.downloadURL
            print(downloadURL)
            
        }
        
    }

    
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        interAd?.show(fromRootViewController: self)
    }
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        print("INTER FAIL!!")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func cropToSquare(image originalImage: UIImage) -> UIImage {
        
        UIGraphicsBeginImageContext(originalImage.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.rotate(by: 0)
        
        originalImage.draw(at: CGPoint(x: 0, y: 0))
        
        let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        //let t = CGAffineTransformMakeRotation(90/(180 * M_PI))
        
        // Create a copy of the image without the imageOrientation property so it is in its native orientation (landscape)
        let contextImage: UIImage = UIImage(cgImage: rotatedImage!.cgImage!)
        
        // Get the size of the contextImage
        let contextSize: CGSize = contextImage.size
        
        let posX: CGFloat
        let posY: CGFloat
        let width: CGFloat
        let height: CGFloat
        
        // Check to see which length is the longest and create the offset based on that length, then set the width and height of our rect
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            width = contextSize.height
            height = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            width = contextSize.width
            height = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: 0, y: 0, width: width, height: height)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        //let image: UIImage = UIImage(CGImage: imageRef, scale: originalImage.scale, orientation: originalImage.imageOrientation)
        let image: UIImage = UIImage(cgImage: imageRef)
        
        return resizeImage(image, newWidth: 600)
        //return scaleImage(image, toSize: CGSize(width: 600, height: 600))
    }
    
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
    func donothing()
    {
        
        
        var nya_listan = ShopList()
        nya_listan.name = "Affär"
        nya_listan.save() {(result: Bool) in
            
        }
        
        nya_listan.addItem(name: "Banan", amount: 3) {(result : Bool) in
            
        }
        
        nya_listan.items![0].shopname = "Apelsin"
        nya_listan.items![0].save() {(result : Bool) in
            
        }
        
        var ny_pryl = ShopItem()
        ny_pryl.my_list = nya_listan
        ny_pryl.shopname = "Häst"
        ny_pryl.amount = 8
        ny_pryl.save() {(result : Bool) in
            
        }
        
        
    }
    
}
