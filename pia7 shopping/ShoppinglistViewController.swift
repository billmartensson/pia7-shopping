//
//  ShoppinglistViewController.swift
//  pia7 shopping
//
//  Created by Bill Martensson on 2017-09-12.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBAudienceNetwork
import FirebaseStorage

import WatchConnectivity

class ShoppinglistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FBAdViewDelegate, WCSessionDelegate {
    
    @IBOutlet weak var additemTextfield: UITextField!
    @IBOutlet weak var additemAmountTextfield: UITextField!
    @IBOutlet weak var shoppingTableview: UITableView!
    
    @IBOutlet weak var bannerView: UIView!
    
    var ref: DatabaseReference!
    
    var shoppingList = ShopList()
    
    var adView : FBAdView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("TEST ID:")
        print(FBAdSettings.testDeviceHash())
        
        //FBAdSettings.addTestDevice("73e8e6affbae002000f72a44fceff5ea4bebe4e9")
        
        
        adView = FBAdView(placementID: "172870736609170_172872266609017", adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        
        adView!.frame = CGRect(x: 0, y: 0, width: bannerView.frame.width, height: bannerView.frame.height)
        
        adView!.delegate = self
        
        adView!.loadAd()
        
        bannerView.addSubview(adView!)
        
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        } else {
            print("WCSession.isSupported NOT TRUE")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        if(Auth.auth().currentUser == nil)
        {
            navigationController?.popViewController(animated: false)
        } else {
            
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            tracker.set(kGAIScreenName, value: "listing")
            
            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker.send(builder.build() as [NSObject : AnyObject])
            
            GAI.sharedInstance().dispatch()

            
            getShopping()
        }
    }
    
    
    
    func getShopping()
    {
        shoppingList.getShoppingItems() {(result: Bool) in
            if(result == true)
            {
                self.shoppingTableview.reloadData()
            } else {
                print("ERROR HÄMTA SHOP ITEMS")
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            
            navigationController?.popToRootViewController(animated: false)
        } catch {
            print("KUNDE INTE LOGGA UT")
        }
    }
    
    
    @IBAction func addItem(_ sender: Any) {
        
        if(Int(additemAmountTextfield.text!) == nil)
        {
            return
        }
        
        if(additemTextfield.text == "")
        {
            print("ERROR INTE TOM LÄGGA TILL")
            
            let alert = UIAlertController(title: "Error!", message: "Name cannot be empty", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
        } else {
            shoppingList.addItem(name: additemTextfield.text!, amount: Int(additemAmountTextfield.text!)!) {(result : Bool) in
                
                if(result == true)
                {
                    self.shoppingTableview.reloadData()
                    
                    self.additemTextfield.text = ""
                    self.additemAmountTextfield.text = ""
                } else {
                    print("ADD ERROR")
                }
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let item_count = shoppingList.items?.count
        {
            return item_count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopping") as! ShoppingItemTableViewCell
        
        cell.itemLabel.text = shoppingList.items![indexPath.row].shopname
        
        if(shoppingList.items![indexPath.row].bought == true)
        {
            cell.itemBought.backgroundColor = UIColor.green
        } else {
            cell.itemBought.backgroundColor = UIColor.clear
        }
        
        shoppingList.items![indexPath.row].getItemImage() {(result : UIImage?) in
            cell.itemImage.image = result
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // MARKERA OM KÖPT
        shoppingList.items![indexPath.row].toggleBought()
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        tableView.reloadRows(at: [indexPath], with: .fade)
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let modifyAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Edit action ...")
            
            self.performSegue(withIdentifier: "shopdetail", sender: indexPath.row)
            
            success(true)
        })
        modifyAction.backgroundColor = .blue
        
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Delete action ...")
            
            self.shoppingList.items![indexPath.row].delete() {(result : Bool) in
                if(result == true)
                {
                    tableView.reloadData()
                } else {
                    print("DELETE ERROR")
                }
            }
            
            
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [modifyAction, deleteAction])
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "shopdetail")
        {
            var destVC = segue.destination as! ShoppingdetailViewController
            
            destVC.s_item = shoppingList.items![sender as! Int]
        }
        
        if(segue.identifier == "login")
        {
            // GÖR INGET
        }
        
    }
 
    
    
    func adViewDidClick(_ adView: FBAdView) {
        print("KLICK PÅ ANNONS")
    }
    
    func adViewDidLoad(_ adView: FBAdView) {
        print("ANNONS LADDAD")
    }
    
    func adView(_ adView: FBAdView, didFailWithError error: Error) {
        print("ANNONS FAIL!")
        print(error.localizedDescription)
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        if(error == nil)
        {
            print("ACTIVATION OK")
            
        } else {
            print("ACTIVATION FAIL")
        }
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        
        if let bought = WCSession.default.receivedApplicationContext["shop_bought"] as? [Bool]
        {
            for (index, si) in self.shoppingList.items!.enumerated() {
                
                if(si.bought != bought[index])
                {
                    print(si.shopname+" HAR BYTT")
                    
                        var addingShopItem = [String : Any]()
                    
                        addingShopItem["bought"] = bought[index]
                    ref.child("pia7shopping").child(shoppingList.owner!).child(shoppingList.fb_key!).child("items").child(shoppingList.items![index].fbKey).updateChildValues(addingShopItem) { (error, ref) in
                        //tableView.reloadRows(at: [indexPath], with: .fade)
                        self.getShopping()
                    }
                }
                
            }
        }
        
    }
    
}
