//
//  pia7_shoppingTests.swift
//  pia7 shoppingTests
//
//  Created by Bill Martensson on 2017-10-03.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import XCTest

class pia7_shoppingTests: XCTestCase {
    
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let buySomething = ShopItem()
        buySomething.shopname = "Banan"
        buySomething.amount = 3
        
        XCTAssertEqual(buySomething.getPrettyAmount(), "3 st")
    
        buySomething.amount = 40
        
        XCTAssertEqual(buySomething.getPrettyAmount(), "40 st")
        
        buySomething.amount = 0
        
        XCTAssertEqual(buySomething.getPrettyAmount(), "")
        
        buySomething.amount = 100
        
        XCTAssertEqual(buySomething.getPrettyAmount(), "Många")
    }
    
    func testToggleShop()
    {
        let buySomething = ShopItem()
        buySomething.bought = true
        buySomething.toggleBought()
        
        XCTAssertFalse(buySomething.bought)
        
        buySomething.toggleBought()
        
        XCTAssertTrue(buySomething.bought)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
            
        }
    }
    
}
